Data processing script for 6.S062

Main functionality: run

python src/convert_inertial_to_traj.py log.csv

where "log.csv" is the name of a csv inertial file containing:

    ax | ay | ax | qw | qx | qy | qz | apple timestamp

outputs two files of the form:

log.csv__pointcloud.txt
log.csv__traj.csv

The former is a pointcloud which can be visualized by importing it into MeshLab.
The latter is a csv file of the form:

    unix timestamp | pos_x | pos_y | vel_x | vel_y | acc_x | acc_y | theta

