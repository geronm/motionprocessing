#!/usr/bin/env python

import sys
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import butter, lfilter, freqz
import matplotlib.pyplot as plt
from extract_data import *

def butter_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a

def butter_lowpass_filter(data, cutoff, fs, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = lfilter(b, a, data)
    return y


# Filter requirements.
order = 6
fs = 100.0 # 30.0      # sample rate, Hz
cutoff = 4.5 # 0.6 # 1.55 # 3.667  # desired cutoff frequency of the filter, Hz

# Get the filter coefficients so we can check its frequency response.
b, a = butter_lowpass(cutoff, fs, order)

# Plot the frequency response.
w, h = freqz(b, a, worN=8000)
plt.subplot(2, 1, 1)
plt.plot(0.5*fs*w/np.pi, np.abs(h), 'b')
plt.plot(cutoff, 0.5*np.sqrt(2), 'ko')
plt.axvline(cutoff, color='k')
plt.xlim(0, 0.5*fs)
plt.title("Lowpass Filter Frequency Response")
plt.xlabel('Frequency [Hz]')
plt.grid()


def numberify(st):
  return float(st.strip().split(':')[-1])

#fn = ('log.csv' if len(sys.argv) < 2 else sys.argv[1])
#
#ff = open(fn, 'r')
#
#ax = []
#ay = []
#az = []
#lsts = [ax,ay,az]
#for l in ff:
#  if ':' in l:
#    nums = [numberify(nst) for nst in l.split(',')]
#    for i in range(len(lsts)):
#      lsts[i].append(nums[i])
#
#x = np.arange(0, len(lsts[0]), 1.0);
#print(x.size)
##y = np.array(lsts[0])
##print(y.size)
#
#norm = []
#for i in range(len(lsts[0])):
#  norm.append(np.linalg.norm([lsts[0][i], lsts[1][i], lsts[2][i]]))

dataname = 'log.csv'
if len(sys.argv) > 1:
  dataname = sys.argv[1]

all_data = extract_data_set(dataname)

ax = np.array(all_data['ax'])
ay = np.array(all_data['ay'])
az = np.array(all_data['az'])
norms = np.sqrt((ax*ax)+(ay*ay)+(az*az))


data = np.array(norms)
# Demonstrate the use of the filter.
# First make some data to be filtered.
## T = 5.0         # seconds
## n = int(T * fs) # total number of samples
n = len(data)
T = float(n)/fs
t = np.linspace(0, T, n, endpoint=False)
# "Noisy" data.  We want to recover the 1.2 Hz signal from this.
#data = np.sin(1.2*2*np.pi*t) + 1.5*np.cos(9*2*np.pi*t) + 0.5*np.sin(12.0*2*np.pi*t)

# Filter the data, and plot both the original and filtered signals.
y = butter_lowpass_filter(data, cutoff, fs, order)

plt.subplot(2, 1, 2)
plt.plot(t, data, 'b-', label='data')
plt.plot(t, y, 'g-', linewidth=2, label='filtered data')
plt.xlabel('Time [sec]')
plt.grid()
plt.legend()

plt.subplots_adjust(hspace=0.35)
plt.show()


#plt.plot(x, np.array(lsts[0]))
#plt.plot(x, np.array(lsts[1]))
#plt.plot(x, np.array(lsts[2]))
#plt.plot(x, np.array(norm))
#plt.plot(x, normlo)
#plt.show()
