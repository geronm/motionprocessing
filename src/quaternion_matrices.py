import numpy as np

def rot_mat_q(w, x, y, z):
  return np.identity(3) + \
      np.array([[-2*y*y-2*z*z, 2*x*y-2*w*z, 2*x*z+2*w*y], \
                [2*x*y+2*w*z, -2*x*x-2*z*z, 2*y*z-2*w*x], \
                [2*x*z-2*w*y, 2*y*z+2*w*x, -2*x*x-2*y*y]])

def q_from_axis_angle(axis, angle):
  # normalize axis if not already
  axis = axis / np.linalg.norm(axis)

  # q = cos Th/2 + a sin Th/2
  a_sin_th_2 = axis * np.sin(angle/2)
  return (np.cos(angle/2), a_sin_th_2[0][0], a_sin_th_2[1][0], a_sin_th_2[2][0])

# code for debug purposes
if __name__=='__main__':
  global qfa, mfq, x, y, z, pi
  qfa = q_from_axis_angle
  mfq = lambda (w,x,y,z) : rot_mat_q(w,x,y,z)

  import math
  pi = math.pi
  x = np.zeros((3,1))
  x[0][0] = 1
  y = np.zeros((3,1))
  y[1][0] = 1
  z = np.zeros((3,1))
  z[2][0] = 1
