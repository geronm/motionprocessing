#Quaternion parser

import sys
import numpy as np
from extract_data import extract_data_set
from quaternion_matrices import rot_mat_q


#Load the data
dataname = 'log.csv'
if len(sys.argv) > 1:
  dataname = sys.argv[1]
all_data = extract_data_set(dataname)

#Get the point cloud described by moving at a constant speed in a direction rotated by the quaternions

#First guess
ori_v = np.array([[1],[0],[0]])

curr_v = np.array([[0],[0],[0]])
curr_x = np.array([[0],[0],[0]])

#Iterate through and output point cloud points
for i in range(len(all_data['qw'])):
  print(' '.join([str(n[0]) for n in curr_x]))
  
  #curr_a = np.array([all_data['ax'][i], all_data['ay'][i], all_data['az'][i]])
  
  w = all_data['qw'][i]
  x = all_data['qx'][i]
  y = all_data['qy'][i]
  z = all_data['qz'][i]
  
  B = rot_mat_q(w,x,y,z)
  
  curr_v = np.dot(B, ori_v)
  curr_x = curr_x + curr_v

