#Quaternion parser

from __future__ import division
import sys
import numpy as np
from extract_data import extract_data_set
from quaternion_matrices import rot_mat_q
from scipy.ndimage.filters import gaussian_filter

#Load the data
dataname = 'log.csv'
if len(sys.argv) > 1:
  dataname = sys.argv[1]

all_data = extract_data_set(dataname)
outputname = dataname + '__pointcloud3d.txt'

#Get the point cloud described by moving at a constant speed in a direction rotated by the quaternions

xname='ax'
yname='ay'
zname='az'
all_a = [np.array([[all_data[xname][i]], [all_data[yname][i]], [all_data[zname][i]]]) for i in range(len(all_data['qw']))]

#Here I correct via gyro
for i in range(len(all_a)):
  w = all_data['qw'][i]
  x = all_data['qx'][i]
  y = all_data['qy'][i]
  z = all_data['qz'][i]
  
  B = rot_mat_q(w,x,y,z)

  all_a[i] = np.dot(B, all_a[i]) # np.dot(np.linalg.inv(B), all_a[i])

#Subtract out gravity (the average accel vector)
gravity_a = np.zeros((3,1))
print(gravity_a.shape)
for i in range(len(all_a)):
  gravity_a = gravity_a + all_a[i]
print(gravity_a.shape)
gravity_a = gravity_a / float(len(all_a))
print(gravity_a.shape)
for i in range(len(all_a)):
#  print(all_a[i])
#  print(gravity_a)
#  print(all_a[i]-gravity_a)
#  print('^yup')
  all_a[i] = all_a[i] - gravity_a
print(gravity_a)

sigma = 5 # 51 * 100 / 30
accel_x = []
accel_y = []
accel_z = []
for a_vec in all_a:
  accel_x.append(a_vec[0][0])
  accel_y.append(a_vec[1][0])
  accel_z.append(a_vec[2][0])
accel_x = gaussian_filter(accel_x, sigma)
accel_y = gaussian_filter(accel_y, sigma)
accel_z = gaussian_filter(accel_z, sigma)
new_all_a = []
for i in range(len(accel_x)):
  new_all_a.append(np.array([[accel_x[i]],[accel_y[i]],[accel_z[i]]]))
#all_a = new_all_a
#for i in range(len(all_a)):
#  all_a[i] = all_a[i] - gravity_a


#Initialize
curr_v = np.zeros((3,1))
curr_x = np.zeros((3,1))

outfile = open(outputname, 'w')

#Iterate through and output point cloud points
T=1
for i in range(len(all_data['qw'])):
  if i%T == 0:
    coordstr = ' '.join([str(n[0]) for n in curr_x])
    # print(coordstr)
    outfile.write(coordstr + '\n')

  local_a = all_a[i]
  world_a = local_a
  
  curr_v = curr_v + .5*world_a
  curr_x = curr_x + curr_v
  curr_v = curr_v + .5*world_a

  curr_v = curr_v * 0.98
  

outfile.close()
