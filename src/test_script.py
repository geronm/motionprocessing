from quaternion_matrices import *
import extract_data as ed
import matplotlib.pyplot as plt
import sys

global qfa, mfq, x, y, z, pi
qfa = q_from_axis_angle
mfq = lambda (w,x,y,z) : rot_mat_q(w,x,y,z)

import math
pi = math.pi
x = np.zeros((3,1))
x[0][0] = 1
y = np.zeros((3,1))
y[1][0] = 1
z = np.zeros((3,1))
z[2][0] = 1

def get_q(all,i):
  return (all['qw'][i],all['qx'][i],all['qy'][i],all['qz'][i])

def get_a(all,i):
  return (all['ax'][i],all['ay'][i],all['az'][i])

#all = ed.extract_data_set('../sample_logs/log_90bouncy_then_back.csv')
if len(sys.argv) < 2:
  all = ed.extract_data_set('../sample_logs/log_gyro_walk_pocket.csv')
else:
  all = ed.extract_data_set(sys.argv[1])
myx = x-y
myx = myx / np.linalg.norm(myx)

def get_vals(all, x):
  xs = []
  for i in range(len(all['qw'])):
    xs.append(np.dot(mfq(get_q(all,i)), x))
  return xs

def plot_vals(xs):
  xp, = plt.plot([xs[i][0][0] for i in range(len(xs))], label='x')
  yp, = plt.plot([xs[i][1][0] for i in range(len(xs))], label='y')
  zp, = plt.plot([xs[i][2][0] for i in range(len(xs))], label='z')
  plt.legend([xp,yp,zp],['xx','yy','zz'])
  plt.show()

#plot_vals(get_vals(all, myx))

accels=[ \
np.array([[all['ax'][i]],[all['ay'][i]],[all['az'][i]]]) \
 for i in range(len(all['ax']))]


accels_rotd = [np.dot(mfq(get_q(all,i)),accels[i]) for i in range(len(accels))]
accels=accels_rotd #TODO GET ME OUT

for axis in (0,1,2):
  print sum([t[axis][0] for t in accels])/len(accels)
#plot_vals(accels)
plot_vals(accels_rotd)
