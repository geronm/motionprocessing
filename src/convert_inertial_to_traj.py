#Quaternion parser

from __future__ import division
import sys
import numpy as np
import math
from extract_data import extract_data_set
from quaternion_matrices import rot_mat_q
from scipy.ndimage.filters import gaussian_filter

# Inertial processing script
#
# The purpose of this script is to process a log file of inertial data
# (obtained via extract_data.extract_data_set) and process it into relevant
# features. Features currently outputted are:
#
#   - A best estimate of the dead-reckoning 2D trajectory of the device,
#       but projected onto a 3D plane (this will change to just X,Y soon)
#
# Anticipated additional features include:
#
#   - A best estimate of the dead-reckoning 3D trajectory of the device
#   - A best estimate of the dead-reckoning 2D trajectory of the device
#       as proper X,Y coordinates
#   - Associated accel and vel for the above
#   - Estimated phone orientation throughout the trajectory, inferred
#       from gyro, gravity, and footstep accel.
#   - STFT time-varying frequency spectra for the inertial readings.
#
# Current stages of data processing are the following:
#
#   - Take as base dataset {a}, the n acceleration vectors
#   - Transform each {a}_i by a {B}_i, the gyro correction factor
#   - Estimate a single gravity direction a_g for the entire run by
#       capturing a vector encompassing the low-frequency components
#       of {a}. This is expected
#       to capture both the frequency-0 gravity signal as well as the
#       low-frequency up/down accelerations due to footsteps
#       (in the future: vary this gravity signal with time, using a window,
#        to account for signal drift)
#    - Project accelerations {a} onto the plane orthogonal to a_g
#       to get the 2D components of motion. This plane is rotated to
#       give a Z-component of 0.
#    - Use finite-difference integration with friction on v to produce
#       position trajectory information.


#Load the data
if len(sys.argv) <= 1:
  print('Usage: \n\n    python ' + str(sys.argv[0]) + ' input_inertial_raw.csv [output_file_name]\n')
  sys.exit(0)

dataname = sys.argv[1]

all_data = extract_data_set(dataname)

outname = dataname
if len(sys.argv) > 2:
  outname = sys.argv[2]
outfile_name_pc = outname + '__pointcloud.txt'
outfile_name_traj = outname + '__traj.csv'

#Get the point cloud described by moving at a constant speed in a direction rotated by the quaternions

xname='ax'
yname='ay'
zname='az'
all_a = [np.array([[all_data[xname][i]], [all_data[yname][i]], [all_data[zname][i]]]) for i in range(len(all_data['qw']))]
all_g = [np.zeros((3,1)) for i in range(len(all_data['qw']))]
if 'gx' in all_data:
  gxname='ax'
  gyname='ay'
  gzname='az'
  all_g = [np.array([[all_data[gxname][i]], [all_data[gyname][i]], [all_data[gzname][i]]]) for i in range(len(all_data['qw']))]

#Here I correct via gyro
for i in range(len(all_a)):
  w = all_data['qw'][i]
  x = all_data['qx'][i]
  y = all_data['qy'][i]
  z = all_data['qz'][i]
  
  B = rot_mat_q(w,x,y,z)

  all_a[i] = np.dot(B, all_a[i]) # np.dot(np.linalg.inv(B), all_a[i])
  all_g[i] = np.dot(B, all_g[i])
    

#If necessary, try to find gravity (the primary average/low-freq accel vector)
gravity_a = np.zeros((3,1))
if 'gx' not in all_data:
  ax = np.array([all_a[i][0][0] for i in range(len(all_a))])
  ay = np.array([all_a[i][1][0] for i in range(len(all_a))])
  az = np.array([all_a[i][2][0] for i in range(len(all_a))])
  axis=0
  for av in [ax,ay,az]:
    gravity_a[axis][0] = \
          np.sum([np.real(freq) for freq in \
                np.fft.fft(av)[0:len(av)/2][0:1]]) # presently just gravity, 0-component of spectrum
    axis += 1
  gravity_a = gravity_a / np.linalg.norm(gravity_a)
else:
  for g_vec in all_g:
    gravity_a += g_vec
  gravity_a = gravity_a / np.linalg.norm(gravity_a)

#Find two vectors proj_x, proj_y. These will be the 3space vectors
# defining the 2space plane onto which our accelerations will
# be projected.
init_0 = np.array([[1],[0],[0]])
init_1 = np.array([[0],[1],[0]])
projx_a = init_0
if abs(np.dot(np.transpose(init_1), gravity_a)) < \
        abs(np.dot(np.transpose(init_0), gravity_a)):
  # for numerical stability; choose the more orthogonal of 1,0,0 & 0,0,1:
  projx_a = init_1
# orthogonalize projx_a W.R.T. gravity_a
projx_a = projx_a - ((np.dot(np.transpose(projx_a),gravity_a))*gravity_a)
projx_a = projx_a / np.linalg.norm(projx_a)
# determine projy_a orthogonal to projx_a
projy_a = np.cross(projx_a, gravity_a, axis=0)

# Project the data
for i in range(len(all_a)):
  a_x = np.dot(np.transpose(all_a[i]),projx_a)[0][0]
  a_y = np.dot(np.transpose(all_a[i]),projy_a)[0][0]
  all_a[i] = np.array([[a_x],[a_y],[0]])


# Smooth the data
sigma = 51 * 100 / 30
accel_x = []
accel_y = []
accel_z = []
for a_vec in all_a:
  accel_x.append(a_vec[0][0])
  accel_y.append(a_vec[1][0])
  accel_z.append(a_vec[2][0])
accel_x = gaussian_filter(accel_x, sigma)
accel_y = gaussian_filter(accel_y, sigma)
accel_z = gaussian_filter(accel_z, sigma)
new_all_a = []
for i in range(len(accel_x)):
  new_all_a.append(np.array([[accel_x[i]],[accel_y[i]],[accel_z[i]]]))
all_a = new_all_a

# Track orientation over time via Gyro
#outfilethetadebug = open(dataname + '__debugtheta.txt','w')
all_theta = [0.] # 0 here is a placeholder; will be removed after loop
for i in range(len(all_data['qw'])):
  # Get the current quaternion
  w = all_data['qw'][i]
  x = all_data['qx'][i]
  y = all_data['qy'][i]
  z = all_data['qz'][i]
  B = rot_mat_q(w,x,y,z)
  
  #Rotate the vector corresponding to (1,0) through 3space
  # to the current heading
  ori_vec3 = np.dot(B, projx_a)
  
  #Project the rotated vector back down to 2D to obtain theta
  ori_vec2_x = np.dot(np.transpose(ori_vec3), projx_a)[0][0]
  ori_vec2_y = np.dot(np.transpose(ori_vec3), projy_a)[0][0]
  
  #(debug purposes) Output theta in pointcloud form TODO
  #if (i%T)==0:
  #  norm = np.linalg.norm(np.array([[s] for s in [ori_vec2_x, ori_vec2_y, 0]]))
  #  outfilethetadebug.write(' '.join([str(x) for x in [ori_vec2_x/norm, ori_vec2_y/norm, 0]]) + '\n')
  
  #Record theta
  if np.abs(ori_vec2_x) < 1E-3 and  np.abs(ori_vec2_y) < 1E-3:
    all_theta.append(all_theta[-1]) # Just keep heading if ori goes singular
  else:
    all_theta.append(math.atan2(ori_vec2_y, ori_vec2_x))
all_theta = all_theta[1:] # remove placeholder 0.
#outfilethetadebug


#Initialize x & v arrays
all_v = []
all_x = []
curr_v = np.zeros((3,1))
curr_x = np.zeros((3,1))
#Iterate through and produce trajectory x & v
for i in range(len(all_data['qw'])):
  all_v += [curr_v]
  all_x += [curr_x]

  world_a = all_a[i]
  
  #Finite difference
  curr_v = curr_v + .5*world_a
  curr_x = curr_x + curr_v
  curr_v = curr_v + .5*world_a
  
  #Friction (for stability)
  curr_v = curr_v * 0.98

#Write out the point cloud file
outfile_pc = open(outfile_name_pc, 'w')
def vec_str(vec):
  return ' '.join([str(n[0]) for n in vec])
T=1 # frequency of output
for i in range(0,len(all_data['qw']),T):
  outfile_pc.write(vec_str(all_x[i]) + '\n')
outfile_pc.close()

#Write out the trajectory csv file
#Format: time(Unix,s) | pos_x | pos_y | vel_x | vel_y | acc_x | acc_y | theta [not impl]
outfile_traj = open(outfile_name_traj, 'w')
outfile_traj.write('ts,pos_x,pos_y,vel_x,vel_y,acc_x,acc_y,theta,mag_vel,K_curve\n') # header
APPLE_TIME_IN_UNIX = float(978307200)
T=1 # frequency of output
for i in range(0,len(all_data['qw']),T):
  outstrings = []
  
  timestamp = all_data['ts'][i] + APPLE_TIME_IN_UNIX
  outstrings += [str(timestamp)]

  pos_x = all_x[i][0][0]
  pos_y = all_x[i][1][0]
  outstrings += [str(pos_x), str(pos_y)]

  vel_x = all_v[i][0][0]
  vel_y = all_v[i][1][0]
  outstrings += [str(vel_x), str(vel_y)]
  
  acc_x = all_a[i][0][0]
  acc_y = all_a[i][1][0]
  outstrings += [str(acc_x), str(acc_y)]

  theta = 0 #TODO: get 2D theta
  outstrings += [str(theta)]

  mag_vel = np.linalg.norm(np.array([vel_x,vel_y]))
  outstrings += [str(mag_vel)]
  
  denom = ((vel_x*vel_x)+(vel_y*vel_y))
  if denom == 0:
    curvature = 0
  else:
    curvature = ((vel_x * acc_y) - (vel_y * acc_x))/np.power(denom,1.5)
  outstrings += [str(curvature)]
  
  outfile_traj.write(','.join(outstrings) + '\n')
outfile_traj.close()
