#!/usr/bin/env python

import sys
import numpy as np
import matplotlib.pyplot as plt
from extract_data import *

def make_vert_plot(data, fs):
  # Code for short FFTs
  # borrowed from KevinNJ: https://github.com/KevinNJ/Projects/tree/master/Short%20Time%20Fourier%20Transform

  # data = a numpy array containing the signal to be processed
  # fs = a scalar which is the sampling frequency of the data
  
  fft_size = 400 # 2*len(data)
  overlap_fac = 0.5

  hop_size = np.int32(np.floor(fft_size * (1-overlap_fac)))
  pad_end_size = fft_size          # the last segment can overlap the end of the data array by no more than one window size
  total_segments = np.int32(np.ceil(len(data) / np.float32(hop_size)))
  t_max = len(data) / np.float32(fs)
   
  window = np.hanning(fft_size)  # our half cosine window
  inner_pad = np.zeros(fft_size) # the zeros which will be used to double each segment size
  
  proc = np.concatenate((data, np.zeros(pad_end_size)))              # the data to process
  result = np.empty((total_segments, fft_size), dtype=np.float32)    # space to hold the result
   
  for i in xrange(total_segments):                      # for each segment
      current_hop = hop_size * i                        # figure out the current segment offset
      segment = proc[current_hop:current_hop+fft_size]  # get the current segment
      windowed = segment * window                       # multiply by the half cosine function
      padded = np.append(windowed, inner_pad)           # add 0s to double the length of the data
      spectrum = np.fft.fft(padded) / fft_size          # take the Fourier Transform and scale by the number of samples
      autopower = np.abs(spectrum * np.conj(spectrum))  # find the autopower spectrum
      result[i, :] = autopower[:fft_size]               # append to the results array
   
  #result = 20*np.log10(result)          # scale to db
  #result = np.clip(result, -40, 200)    # clip values

  img = plt.imshow(result, origin='lower', cmap='jet', interpolation='nearest', aspect='auto')
  plt.show()


dataname = 'log.csv'
if len(sys.argv) > 1:
  dataname = sys.argv[1]

all_data = extract_data_set(dataname)

ax = np.array(all_data['ax'])
ay = np.array(all_data['ay'])
az = np.array(all_data['az'])
norms = np.sqrt((ax*ax)+(ay*ay)+(az*az))

print(norms[:100])
print(max(norms))

data = norms
fs = 100

# start at ~5.25 s
# stop at ~17 s

#data = data[650:1600]
print(len(data))
#
#plt.plot(np.abs(np.fft.fft(data)[1:len(data)/2]))
#plt.plot(data)
def window_avg(narr, w):
  return np.convolve(narr, np.array([1.0/w]*w))
for av in [ax,ay,az]:
  plt.plot(np.abs(np.fft.fft(av)[0:len(av)/2]))
  print(np.sum(np.abs(np.fft.fft(av)[0:len(av)/2])[0:1]))
#plt.plot(np.array(all_data['ts']))
#
plt.show()

#make_vert_plot(data, fs)
