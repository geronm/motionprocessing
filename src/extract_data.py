
def extract_data_set(dataname):

  data_list = None
  begin = True
  ff = open(dataname)
  column_head_list = 'ax,ay,az,qx,qy,qz,qw,ts'.split(',')
  column_head_dict = {i:column_head_list[i] for i in range(len(column_head_list))}
  for l in ff:
    if begin and (not l[0] in ['.','0','1','2','3','4','5','6','7','8','9']):
      column_head_list = l.strip().split(',')
      print column_head_list
      column_head_dict = {i:column_head_list[i] for i in range(len(column_head_list))}
      begin = False
      continue
    begin = False
    
    #We have a line of genuine data
    broken_up_data = [float(s) for s in l.strip().split(',')]
    if data_list is None:
      data_list = [list() for _ in broken_up_data]
    assert len(broken_up_data) == len(data_list), (("Data suddenly changed size. Should've been %d but line was " + l) % len(data_list))
    for i in range(len(data_list)):
      data_list[i].append(broken_up_data[i])
  
  all_data = {}
  for i in range(len(data_list)):
    all_data[column_head_dict[i]] = data_list[i]

  return all_data
  
  
# Test

if __name__=='__main__':
  import sys
  ad = extract_data_set(sys.argv[1])
  print ad.keys()

